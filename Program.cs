using System;
using System.IO;
using System.Text;
using MySql.Data.MySqlClient;

namespace Rainfall_datafile_parser
{
    class Program
    {
        static void Main(string[] args)
        {
            // Declare connetion string to the database and create the connection using mysql data Nuget package
            MySqlConnection databaseConnection = new MySqlConnection("Database=rainfalldata;datasource=localhost;port=3306;User Id=RainParser;password=Vbg>f#5JPx]d9w]");
            
            // initialise required variables 
            StringBuilder query = new StringBuilder();

            bool finishedHeader = false;
            string path = "";

            int startYear = 0;
            int endYear;

            int xref = 0;
            int yref = 0;
            int currentYear = 0;
            int currentMonth = 0;

            while (true)
            {
                Console.WriteLine("Enter the path to the file to read into the database");
                path = Console.ReadLine();
                if (File.Exists(path))
                {
                    break;
                }

                else
                {
                    Console.WriteLine("Enter a valid path");
                }
            }
            var dataFile = File.ReadLines(path);

            foreach (string line in dataFile)
            {
                if (!finishedHeader)
                {
                    if (line.Contains("Years="))
                    {
                        line.IndexOf("Years=");
                        string[] years = line.Substring(line.IndexOf("Years="), line.IndexOf(" [Multi="))
                            .Split('=')
                            [1].Split(']')
                            [0].Split('-');
                        if (!int.TryParse(years[0], out startYear))
                        {
                            Console.WriteLine("An error has occured, there is erroneous data in line {}", line);
                            throw new InvalidCastException();
                        }

                        if (!int.TryParse(years[1], out endYear))
                        {
                            Console.WriteLine("An error has occured, there is erroneous data in line {}", line);
                            throw new InvalidCastException();
                        }
                        finishedHeader = true;
                    }
                    continue;
                }

                if (line.Substring(0, 8) == "Grid-ref")
                {
                    finishedHeader = true;
                    string[] gridRef = line.Split('=')[1].Split(',');
                    if (!int.TryParse(gridRef[0].Trim(' '), out xref))
                    {
                        Console.WriteLine("An error has occured, there is erroneous data in line {}",line);
                        throw new InvalidCastException();
                    }

                    if (!int.TryParse(gridRef[1].Trim(' '), out yref))
                    {
                        Console.WriteLine("An error has occured, there is erroneous data in line {}", line);
                        throw new InvalidCastException();
                    }

                    currentYear = startYear;
                    currentMonth = 1;

                    if (!string.IsNullOrEmpty(query.ToString()))
                    {
                        try
                        {
                            databaseConnection.Open();
                            MySqlCommand comm = databaseConnection.CreateCommand();
                            comm.CommandText = query.ToString();
                            comm.ExecuteReader();
                            databaseConnection.Close();
                        }
                        catch (Exception e)
                        {
                            Console.WriteLine("An error has occured while writing the data to the database:\n {0}\n press any key to continue", e.ToString());
                            Console.ReadKey();
                        }
                        query.Clear();  
                    }
                }
                else
                {
                    
                    var value = "";
                    for (int i = 0; i < line.Length -1; i += 5)
                    {
                        if (i < 6)
                        {
                            value = line.Substring(0, 5);
                        }
                        else
                        {
                            value = line.Substring(i-5, 5);
                        }
                       ;
                        string date = "";
                        if (currentMonth.ToString().Length == 1)
                        {
                            date = "01-0"+ currentMonth.ToString() + "-"+ currentYear.ToString();
                        }
                        else
                        {
                            date = "01-" + currentMonth.ToString() + "-" + currentYear.ToString();
                        }
                        currentMonth += 1;
                        query.AppendFormat(
                            "INSERT INTO rainfalldata.rainfall (`xref`,`yref`,`date`,`value`) VALUES ({0},{1},'{2}',{3});",
                            xref,
                            yref,
                           (date + " 00:00:00"),
                            value
                        );
                    }
                }
                currentYear += 1;
            }

            try
            {
                MySqlCommand comm = databaseConnection.CreateCommand();
                comm.CommandText = query.ToString();
                comm.ExecuteReader();
                Console.WriteLine("Your file has been added to the database");
            }
            catch (Exception e)
            {
                Console.Write("An error has occured while writing the data to the database:\n {0}", e.ToString());
            }
            Console.ReadKey();
        }
    }
}
