﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Rainfall_datafile_parser
{
    public class Globals
    {
        public static Dictionary<string, string> ParamDictionary { get; set; } = new Dictionary<string, string>();
    }
}
